package io.choerodon.test.manager.infra.constant;

/**
 * Copyright (c) 2022. Hand Enterprise Solution Company. All right reserved.
 *
 * @author zongqi.hao@zknow.com
 * @since 2022/8/2
 */
public interface ExcelSheetConstants {

    String TEST_CASE_SHEET_NAME = "测试用例";


}
