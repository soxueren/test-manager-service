package io.choerodon.test.manager.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import io.choerodon.test.manager.infra.dto.ListLayoutDTO;

/**
 * @author zhaotianxin
 * @date 2021-05-07 14:09
 */
public interface ListLayoutMapper extends BaseMapper<ListLayoutDTO> {
}
