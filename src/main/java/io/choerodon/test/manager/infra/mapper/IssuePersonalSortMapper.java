package io.choerodon.test.manager.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import io.choerodon.test.manager.infra.dto.TestPersonalSortDTO;

/**
 * @author superlee
 * @since 2021-10-27
 */
public interface IssuePersonalSortMapper extends BaseMapper<TestPersonalSortDTO> {
}
