package io.choerodon.test.manager.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import io.choerodon.test.manager.infra.dto.ListLayoutColumnRelDTO;

/**
 * @author zhaotianxin
 * @date 2021-05-07 14:10
 */
public interface ListLayoutColumnRelMapper extends BaseMapper<ListLayoutColumnRelDTO> {
}
